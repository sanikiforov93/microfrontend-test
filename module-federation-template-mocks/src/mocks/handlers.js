import { http } from 'msw';
import { getCounterHistory, addCounterHistory } from './counter/counter.resolver';

export const handlers = [
    http.get('api/counter/history', getCounterHistory),
    http.post('api/counter/history/add', addCounterHistory),
];
