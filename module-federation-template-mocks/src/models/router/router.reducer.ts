import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { Page } from '@models/router/router.types';

const initialState: { page: Nullable<Page> } = {
    page: Page.Home,
};

export const routerSlice = createSlice({
    name: 'router',
    initialState,
    reducers: {
        setPage: (state, action: PayloadAction<Page>) => {
            state.page = action.payload;
        },
    },
});

export const selectPage = (state: RootState) => state.router.page;

export const routerReducer = routerSlice.reducer;
