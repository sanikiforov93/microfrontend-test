import { Page } from '@models/router/router.types';
import { Home } from '@features/home/home.component';
import { ElementType } from 'react';

export const pages: Record<Page, ElementType> = {
    [Page.Home]: Home,
};
