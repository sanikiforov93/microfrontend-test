import { ElementType } from 'react';
import { useAppSelector } from '@app/hooks/hooks';
import { selectPage } from '@models/router/router.reducer';
import { pages } from './router.constants';

export function Router() {
    const page = useAppSelector(selectPage);
    
    if (page === null) return null;
    const ComponentPage = pages[page];
    return <ComponentPage />;
}
