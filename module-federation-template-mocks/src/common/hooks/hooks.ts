import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import type { RootState, AppDispatch } from '../../models/store';
import { useCallback } from 'react';

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useActionDispatch = <T>(action: any) => {
    const dispatch = useAppDispatch();
    const dispatchAction = useCallback(
        (payload: T) => {
            dispatch(action(payload));
        },
        [dispatch, action],
    );
    return dispatchAction;
};
