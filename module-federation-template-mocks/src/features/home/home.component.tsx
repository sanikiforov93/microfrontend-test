import { Counter } from '@features/counter/counter.component';
import React, { memo } from 'react';

const HomeComponent = () => {
    return <Counter />;
};

export const Home = memo(HomeComponent);
