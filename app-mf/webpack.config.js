const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ModuleFederationPlugin = webpack.container.ModuleFederationPlugin;

const production = process.env.NODE_ENV === 'production';
const isDevMode = process.env.NODE_ENV === 'development';
const resolvePath = p => path.resolve(__dirname, p)
const deps = require("./package.json").dependencies;

module.exports = {
    entry: { myAppName: path.resolve(__dirname, "./src/index.js") },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: production ? '[name].[contenthash].js' : '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [isDevMode && require.resolve('react-refresh/babel')].filter(Boolean),
                        },
                    },
                ],
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [isDevMode && require.resolve('react-refresh/babel')].filter(Boolean),
                        },
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                        },
                    },
                ],
            },

            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            import: false,
                        },
                    },
                ],
            },
            {
                test: /\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$/,
                type: 'asset/resource'
            },
            {
                test: /\.svg$/,
                use: [
                  {
                    loader: '@svgr/webpack',
                    options: {
                      prettier: false,
                      svgo: false,
                      svgoConfig: {
                        plugins: [{ removeViewBox: false }]
                      },
                      titleProp: true,
                      ref: true
                    }
                  },
                  {
                    loader: 'file-loader',
                    options: {
                      name: 'static/media/[name].[hash].[ext]'
                    }
                  }
                ],
                issuer: {
                  and: [/\.(ts|tsx|js|jsx|md|mdx)$/]
                }
            }

        ],
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
        alias: {
          '@features': resolvePath('./src/features'),
          '@components': resolvePath('./src/components'),
          '@app': resolvePath('./src/app'),
          '@images': resolvePath('./public/images'),
          '@models': resolvePath('./src/models'),
        },
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: "Webpack & React",
            template: "./public/index.html",
        }),
        new MiniCssExtractPlugin({
            filename: production ? '[name].[contenthash].css' : '[name].css',
        }),
        new ModuleFederationPlugin({
          name: "counter",
          filename: "remoteEntry.js",
          library: { type: 'var', name: 'counter' },
          exposes: {
              "./CounterComponent": "./src/app/integration/init.tsx",
          },
          shared: {
            react: {
              eager: true,
              singleton: true,
              requiredVersion: deps.react,
            },
            "react-dom": {
              eager: true,
              singleton: true,
              requiredVersion: deps["react-dom"],
            },
            "styled-components": {
              eager: true,
              singleton: true,
            },
          },
        }),
    ],
    devServer: {
        port: 3001,
        hot: true,
        open: true,
    },
    optimization: {
      splitChunks: false,
    },
    mode: production ? 'production' : 'development'
};
