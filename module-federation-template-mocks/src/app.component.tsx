import React, { useEffect } from 'react';
import { Router } from '@common/router/router.component';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { tCallback } from '@public/m-integrations/m-interactions.types';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            retry: false,
            refetchOnWindowFocus: false,
        },
    },
});

interface AppProps {
    userId?: string;
    mainCallback?: tCallback;
}

function App({ userId, mainCallback }: AppProps) {
    
    useEffect(() => {
        if (userId) {
        console.log(userId);
        }
        if (mainCallback) {
            mainCallback(undefined, [{ name: "pageName", value: "COUNTER MOCK!!!" }])
        }
    }, [userId, mainCallback]);


    return (
        <QueryClientProvider client={queryClient}>
            <Router />
        </QueryClientProvider>
    );
}

export default App;
