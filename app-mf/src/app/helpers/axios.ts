import axios, { AxiosInstance } from "axios";

class Axios {
    constructor() {
        this.axiosInstance = axios.create();
    }

    public axiosInstance: AxiosInstance;
}
export const axiosApi = new Axios();
