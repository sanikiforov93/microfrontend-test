import { createAsyncThunk } from '@reduxjs/toolkit';
import { fetchCount, getCounterHistoryApi } from './counter.api';

export const incrementAsync = createAsyncThunk('counter/fetchCount', async (amount: number) => {
    const response = await fetchCount(amount);
    return response.data;
});

export const getCounterHistory = createAsyncThunk('counter/history', async () => {
    return await getCounterHistoryApi();
});
