import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '@features/counter/counter.reducer';
import { routerReducer } from '@models/router/router.reducer';

export const store = configureStore({
    reducer: {
        counter: counterReducer,
        router: routerReducer,
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
