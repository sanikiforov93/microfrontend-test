import { HttpResponse } from 'msw';

const counterHistory = [];

export const getCounterHistory = () => {
    return HttpResponse.json(counterHistory);
};

export const addCounterHistory = async ({ request }) => {
    const amountData = await request.json();
    console.log(amountData);
    counterHistory.push(amountData?.amount);
    return HttpResponse.json(amountData, { status: 201 });
};
