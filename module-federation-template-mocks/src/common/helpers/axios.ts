import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

class Axios {
    constructor() {
        this.axiosInstance = axios.create();
    }

    public init(headers: Record<string, string>) {
        const config: AxiosRequestConfig = {
            headers: headers,
        };
        this.axiosInstance = axios.create(config);
    }

    public axiosInstance: AxiosInstance;
}
export const axiosApi = new Axios();
