import { axiosApi } from '@common/helpers';

// A mock function to mimic making an async request for data
export function fetchCount(amount = 1) {
    return new Promise<{ data: number }>((resolve) => setTimeout(() => resolve({ data: amount }), 500));
}

export async function getCounterHistoryApi() {
    const response = await axiosApi.axiosInstance.get('/api/counter/history');
    return response.data;
}

export async function addCounterHistoryApi(amount: number) {
    await axiosApi.axiosInstance.post('/api/counter/history/add', { amount });
}
