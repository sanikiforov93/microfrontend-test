export enum ACTIVITIES {
    Start = "START",
    Close = "CLOSE",
  };
  
export type tParams = { name: string; value: string | boolean }[];

export type tCallback = (
        activityId?: ACTIVITIES,
        params?: tParams
    ) => void;
