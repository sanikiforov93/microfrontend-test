import React, { useEffect } from 'react';
import { Router } from '@components/router/router.component';

interface AppProps {
  mdmId?: string;
}

function App({ mdmId }: AppProps) {

  useEffect(() => {
    if (mdmId) {
      alert(mdmId);
    }
  }, [mdmId]);

  return (<Router />);
}

export default App;
