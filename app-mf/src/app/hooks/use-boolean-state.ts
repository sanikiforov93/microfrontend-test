import { useState, useCallback } from 'react';

export const useBooleanState = (defaultState: boolean) => {
    const [value, setValue] = useState(defaultState);

    const handleSetTrue = useCallback(() => {
        setValue(true);
    }, []);

    const handleSetFalse = useCallback(() => {
        setValue(false);
    }, []);

    const handleToggle = useCallback(() => {
        setValue((currentValue) => !currentValue);
    }, []);

    return {
        value,
        setTrue: handleSetTrue,
        setFalse: handleSetFalse,
        toggle: handleToggle,
    };
};
