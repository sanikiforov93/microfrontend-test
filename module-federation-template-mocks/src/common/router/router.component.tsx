import React from 'react';
import { selectPage } from '@models/router/router.reducer';
import { pages } from './router.constants';
import { useAppSelector } from '@common/hooks/hooks';

export function Router() {
    const page = useAppSelector(selectPage);

    if (page === null) return null;
    const ComponentPage = pages[page];
    return <ComponentPage />;
}
