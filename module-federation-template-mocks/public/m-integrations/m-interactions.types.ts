  
  export enum ACTIVITIES {
    Start = "START",
    Close = "CLOSE",
  };
  
  type tParams = { name: string; value: string | boolean }[];
  
  export type tCallback = (
    activityId?: ACTIVITIES,
    params?: tParams
  ) => void;
  