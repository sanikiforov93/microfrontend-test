export interface CounterState {
    value: number;
    history: number[];
    status: 'idle' | 'loading' | 'failed';
}
