import React, { useEffect, useState } from 'react';

import { useAppSelector, useAppDispatch, useActionDispatch } from '@common/hooks/hooks';
import {
    decrement,
    increment,
    incrementByAmount,
    incrementIfOdd,
    selectCount,
    selectCounterHistory,
} from './counter.reducer';
import { incrementAsync, getCounterHistory } from './counter.thunks';
import { StyledRow, StyledValue, StyledButton, StyledAsyncButton, StyledTextbox } from './counter.styles';
import { useMutation } from '@tanstack/react-query';
import { addCounterHistoryApi } from './counter.api';

export function Counter() {
    const count = useAppSelector(selectCount);
    const counterHistory = useAppSelector(selectCounterHistory);
    const add = useActionDispatch(increment);
    const getHistory = useActionDispatch(getCounterHistory);
    const dispatch = useAppDispatch();

    const { mutate: addHistoryList } = useMutation({
        mutationKey: [addCounterHistoryApi.name],
        mutationFn: addCounterHistoryApi,
        onSuccess: getHistory,
    });

    const [incrementAmount, setIncrementAmount] = useState('2');

    useEffect(() => {
        addHistoryList(count);
    }, [count, addHistoryList]);

    const incrementValue = Number(incrementAmount) || 0;

    return (
        <div>
            <ul>
                {counterHistory.map((it, index) => (
                    <li key={index}>{it}</li>
                ))}
            </ul>
            <StyledRow>
                <StyledButton aria-label="Decrement value" onClick={() => dispatch(decrement())}>
                    -
                </StyledButton>
                <StyledValue>{count}</StyledValue>
                <StyledButton aria-label="Increment value" onClick={add}>
                    +
                </StyledButton>
            </StyledRow>
            <StyledRow>
                <StyledTextbox
                    aria-label="Set increment amount"
                    value={incrementAmount}
                    onChange={(e) => setIncrementAmount(e.target.value)}
                />
                <StyledButton onClick={() => dispatch(incrementByAmount(incrementValue))}>Add Amount</StyledButton>
                <StyledAsyncButton onClick={() => dispatch(incrementAsync(incrementValue))}>
                    Add Async
                </StyledAsyncButton>
                <StyledButton onClick={() => dispatch(incrementIfOdd(incrementValue))}>Add If Odd</StyledButton>
            </StyledRow>
        </div>
    );
}
