import React, { memo, useState } from 'react';
import init from "counter/CounterComponent";
import { ACTIVITIES, tCallback, tParams } from '@app/common.types';

const HomeComponent = () => {

  const [pageName, setPageName] = useState<string>('')

  const callback: tCallback = (_activityId?: ACTIVITIES, params?: tParams) => {
    const currentPageName = (params || []).find(it => it.name === "pageName");
    if(currentPageName && typeof currentPageName.value === 'string') {
      setPageName(currentPageName.value);
    }
  }

  const initCounter = (ref: HTMLDivElement) => {
    if (ref) {
      const coenterInit = init(ref, { clientSessionId: "123",  callback });
      coenterInit.callback();
      
    }
  };
  
  return (
    <>
      <h1>Контейнер {pageName}</h1>
      <div ref={initCounter} />
    </>
  );
}

export const Home = memo(HomeComponent)
