import * as React from "react";
import * as ReactDOM from "react-dom";

import {
  tCallback,
} from "./integration.types";

import App from '../../app.component';
import { Provider } from 'react-redux';
import { store } from '../../models/store';
import { createRoot } from 'react-dom/client';

function init(
  node: HTMLElement | null,
  config?: {
    clientSessionId: string;
    callback: tCallback;
  }
) {
  const { clientSessionId, callback: mainCallback } = config || {};


  const callback: tCallback = (activityId, params) => {
    if (node !== null) {
      console.log(activityId);
      console.log(params);
      const root = createRoot(node);

      root.render(<React.StrictMode>
        <Provider store={store}>
          <App userId={clientSessionId} mainCallback={mainCallback} />
        </Provider>
      </React.StrictMode>);
    }

    return {
      result: [
        {
          exitCode: 200,
        },
      ],
    };
  };

  return { callback };
}

export default init;
