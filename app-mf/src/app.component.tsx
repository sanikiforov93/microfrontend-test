import React, { useEffect } from 'react';
import { Router } from '@components/router/router.component';
import { tCallback } from '@app/mo-integrations/moInteractions.types';

interface AppProps {
  userId?: string;
  mainCallback?: tCallback;
}

function App({ userId, mainCallback }: AppProps) {

  useEffect(() => {
    if (userId) {
      console.log(userId);
    }
    if (mainCallback) {
      mainCallback(undefined, [{ name: "pageName", value: "COUNTER!!!" }])
    }
  }, [userId, mainCallback]);

  return (<Router />);
}

export default App;
